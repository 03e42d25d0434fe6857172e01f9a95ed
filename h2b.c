#define _GNU_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>

int main() {
  char ibuf[BUFSIZ], obuf[BUFSIZ*4];
  const char d2b[16*4] = "0000" "0001" "0010" "0011"
                         "0100" "0101" "0110" "0111"
                         "1000" "1001" "1010" "1011"
                         "1100" "1101" "1110" "1111";
  const char h2d[] = {
    ['0'] =  0*4, ['1'] =  1*4, ['2'] =  2*4, ['3'] =  3*4, ['4'] =  4*4,
    ['5'] =  5*4, ['6'] =  6*4, ['7'] =  7*4, ['8'] =  8*4, ['9'] =  9*4,
    ['A'] = 10*4, ['B'] = 11*4, ['C'] = 12*4, ['D'] = 13*4, ['E'] = 14*4, ['F'] = 15*4,
    ['a'] = 10*4, ['b'] = 11*4, ['c'] = 12*4, ['d'] = 13*4, ['e'] = 14*4, ['f'] = 15*4,
  };

  int size, i, o;
  posix_fadvise(0, 0, 0, POSIX_FADV_SEQUENTIAL);

  while ((size = read(0, ibuf, BUFSIZ)) == BUFSIZ) {
    for (i = o = 0; i < BUFSIZ; i += 4, o += 16) {
      memcpy(obuf + o     , &d2b[h2d[ibuf[i  ]]], 4);
      memcpy(obuf + o +  4, &d2b[h2d[ibuf[i+1]]], 4);
      memcpy(obuf + o +  8, &d2b[h2d[ibuf[i+2]]], 4);
      memcpy(obuf + o + 12, &d2b[h2d[ibuf[i+3]]], 4);
    }
    write(1, obuf, BUFSIZ * 4);
  }

  if (size) {
    for (i = o = 0; i < size; i++, o += 4)
      memcpy(obuf + o, &d2b[h2d[ibuf[i]]], 4);
    write(1, obuf, size * 4);
  }
  return 0;
}
